import random as r
import os
from time import sleep
from json import dumps
from kafka import KafkaProducer
from prometheus_client import start_http_server, Summary
from os import path
import yaml
from prometheus_client.core import GaugeMetricFamily, REGISTRY, CounterMetricFamily
import time

#bid = []
#ask = []
#count = 0
#bid1 = 0
#ask1 = 0
totalRandomNumber = 0
def randmy(start, end):
    return r.randint(start, end)

producer = KafkaProducer(bootstrap_servers=['broker:29092'],
                         value_serializer=lambda x: 
                         dumps(x).encode('utf-8'))

def Average(lst):
    return sum(lst) / len(lst)

def run_app():
    while(True):
        count = 10
        bid = []
        ask = []
        for i in range(50):
            bid.append(randmy(1, count))
            ask.append(randmy(1, count))
            #bid1 = Average(bid)
            #ask1 = Average(ask)
            if count == 50:
                bid1 = Average(bid)
                ask1 = Average(ask)

    #print('bid', bid)
    #print('ask', ask)
    #print(len(bid))
    #print(len(ask))
    #for e in bid1:
                data = {'bid' : bid1}
                producer.send('numtest', value=data)
                data = {'ask' : ask1}
                producer.send('numtest', value=data)

                bid.clear()
                ask.clear()
            
            count += 10
        time.sleep(0.001)
    return bid1 + ask1

class RandomNumberCollector(object):
    def __init__(self):
        pass
    def collect(self):
        gauge = GaugeMetricFamily("random_number", "bid", labels=["randomNum"])
        gauge.add_metric(['random_num'], bid1)
        yield gauge
        count = CounterMetricFamily("random_number_2", "ask", labels=['randomNum'])
        global totalRandomNumber
        totalRandomNumber = run_app()
        totalRandomNumber = run_app()
        count.add_metric(['random_num'], totalRandomNumber)
        yield count

if __name__ == "__main__":
    start_http_server(9000)
    REGISTRY.register(RandomNumberCollector())
    while True: 
        # period between collection
        time.sleep(1)
#for e in bid:
#    data = {'bid' : e}
#    producer.send('numtest', value=data)
#sleep(5)
