kafka-python
Flask>=1.1.2
Flask-WTF>=0.14.3
flask-caching>=1.10.1
psycopg2-binary>=2.8.6
WTForms>=2.3.3
prometheus-client
pyyaml
