FROM python:latest

RUN apt-get update && apt-get install -y iputils-ping net-tools

COPY app.py /usr/local/share/

COPY ./requirements.txt /usr/local/share/

RUN pip install -r /usr/local/share/requirements.txt

CMD [ "python", "/usr/local/share/app.py" ]




#FROM python:3

#ADD app.py /

#RUN pip install pystrich

#CMD [ "python", "./app.py" ]
